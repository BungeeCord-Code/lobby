package net.souppvp.events;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import net.souppvp.Lobby;
import net.souppvp.units.MySQL;
import net.souppvp.units.ScoreBoardManager;
import net.souppvp.units.hider;
import net.souppvp.units.inv;
import net.souppvp.units.lobbymanager;
import net.souppvp.units.nick;

public class Join implements Listener{
	
	@EventHandler
	public void onJoin(PlayerJoinEvent e){
		Player p = e.getPlayer();
		e.setJoinMessage(null);
		if(!lobbymanager.UserExists(p.getUniqueId().toString())){
			try {
				PreparedStatement ps = MySQL.getConnection().prepareStatement("INSERT INTO settings(UUID, hider, nick) VALUES (?, 0, 0)");
				ps.setString(1, p.getUniqueId().toString());
				ps.executeUpdate();
				Lobby.nick.put(p, "�cAus");
				hider.mode.put(p, 0);
			} catch (SQLException e2) {
				e2.printStackTrace();
			}
		} else {
			if(lobbymanager.getnick(p.getUniqueId().toString()) == 0){
				Lobby.nick.put(p, "�cAus");
			} else if(lobbymanager.getnick(p.getUniqueId().toString()) == 1){
				Lobby.nick.put(p, "�aAn");
			}
			if(hider.mode.containsKey(p)){
				hider.mode.remove(p);
			}
			hider.mode.put(p, lobbymanager.gethider(p.getUniqueId().toString()));
			nick.mode.put(p, lobbymanager.getnick(p.getUniqueId().toString()));
		}
		for(Player all : Bukkit.getOnlinePlayers()){
			hider.hide(all);
		}
		ScoreBoardManager.setsb(p);
		inv.JoinItems(p);
	}
	

}
