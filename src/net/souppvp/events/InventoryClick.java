package net.souppvp.events;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;

import net.souppvp.Lobby;
import net.souppvp.units.gameselector;

public class InventoryClick implements Listener{

	Lobby plugin;
	
	public InventoryClick(Lobby plugin) {
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onInventoryClick(InventoryClickEvent e) {
		Player p = (Player) e.getWhoClicked();
		e.setCancelled(true);
		if(e.getInventory().getTitle().contains("Lobby")){
			if(e.getCurrentItem() == null | e.getCurrentItem().getType() == Material.AIR){
				return;
			} else {
				String server = e.getCurrentItem().getItemMeta().getDisplayName().replace("�a", "");
				ByteArrayDataOutput out = ByteStreams.newDataOutput();
				out.writeUTF("Connect");
				out.writeUTF(server);
				p.sendPluginMessage(plugin, "BungeeCord", out.toByteArray());
			}
		} else if(e.getInventory().getTitle().equalsIgnoreCase("�aTeam-QSG")){
			if(e.getCurrentItem() == null | e.getCurrentItem().getType() == Material.AIR){
				return;
			} else {
				if(e.getCurrentItem().getItemMeta().getDisplayName().contains("Zur�ck")) {
					return;
				} else if(e.getCurrentItem().getItemMeta().getDisplayName().contains("Ingame")) {
					gameselector.ingameopengameservers(p, "Team-QSG");
				} else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("�cTeam-QSG")) {
					return;
				} else {
					String server = e.getCurrentItem().getItemMeta().getDisplayName().replace("�a", "").replace("�6", "");
					ByteArrayDataOutput out = ByteStreams.newDataOutput();
					out.writeUTF("Connect");
					out.writeUTF(server);
					p.sendPluginMessage(plugin, "BungeeCord", out.toByteArray());
				}
			}
		} else if(e.getInventory().getTitle().equalsIgnoreCase("�cINGAME �7| �aTeam-QSG")){
			if(e.getCurrentItem() == null | e.getCurrentItem().getType() == Material.AIR){
				return;
			} else {
				if(e.getCurrentItem().getItemMeta().getDisplayName().contains("Zur�ck")) {
					gameselector.opengameservers(p, "Team-QSG");
				} else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("�cTeam-QSG")) {
					return;
				} else {
					String server = e.getCurrentItem().getItemMeta().getDisplayName().replace("�4", "");
					ByteArrayDataOutput out = ByteStreams.newDataOutput();
					out.writeUTF("Connect");
					out.writeUTF(server);
					p.sendPluginMessage(plugin, "BungeeCord", out.toByteArray());
				}
			}
		}
	}
	
}
