package net.souppvp.events;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import net.souppvp.Lobby;
import net.souppvp.units.hider;

public class Quit implements Listener{
	
	@EventHandler
	public void onQuit(PlayerQuitEvent e){
		e.setQuitMessage(null);
		Lobby.nick.remove(e.getPlayer());
		hider.mode.remove(e.getPlayer());
	}

}
