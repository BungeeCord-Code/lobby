package net.souppvp.events;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

import net.souppvp.units.gameselector;
import net.souppvp.units.hider;
import net.souppvp.units.lobbyswitcher;
import net.souppvp.units.nick;

public class Interact implements Listener{

	@EventHandler
	public void onInteract(PlayerInteractEvent e){
		Player p = e.getPlayer();
		e.setCancelled(true);
		if(e.getAction() == Action.RIGHT_CLICK_BLOCK | e.getAction() == Action.RIGHT_CLICK_AIR) {
			if(p.getItemInHand().getType().equals(Material.INK_SACK)){
				if(p.getItemInHand().getItemMeta().getDisplayName().contains("Spieler") | p.getItemInHand().getItemMeta().getDisplayName().contains("VIP")){
					hider.updatehider(p);
				}
			} else if(p.getItemInHand().getType().equals(Material.NAME_TAG)){
				if(p.getItemInHand().getItemMeta().getDisplayName().contains("AutoNick")){
					nick.updatenick(p);
				}
			} else if(p.getItemInHand().getType().equals(Material.EMERALD)){
				if(p.getItemInHand().getItemMeta().getDisplayName().equalsIgnoreCase("�aLobby-Switcher")){
					lobbyswitcher.openlobbyselector(p);
				}
			} else if(p.getItemInHand().getType().equals(Material.CHEST)){
				if(p.getItemInHand().getItemMeta().getDisplayName().equalsIgnoreCase("�cSpielmodie")){
					gameselector.opengameservers(p, "Team-QSG");
				}
			}
		}
	}
	
}
