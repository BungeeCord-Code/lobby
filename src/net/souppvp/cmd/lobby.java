package net.souppvp.cmd;

import java.io.File;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import net.souppvp.Lobby;

public class lobby implements CommandExecutor{

	Lobby plugin;
	
	public lobby(Lobby plugin) {
		this.plugin = plugin;
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String lable, String[] args) {
		Player p = (Player) sender;
		if(p.hasPermission("lobby.admin")){
			if(args.length == 2){
				try {
					File file = new File(plugin.getDataFolder().getPath(), "locations.yml");
					FileConfiguration locations = YamlConfiguration.loadConfiguration(file);
					if(args[0].equalsIgnoreCase("set")){
						locations.set(args[1], p.getLocation());
						locations.save(file);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} else {
			p.sendMessage("�8�m-----------------------------------------");
			p.sendMessage("            �aLobby-System");
			String author = plugin.getDescription().getAuthors().get(0);
			for (int i = 1; i < plugin.getDescription().getAuthors().size(); i++) {
				author = author + " & "+plugin.getDescription().getAuthors().get(i);
			}
			p.sendMessage("  �7von�8:�e "+author);
			p.sendMessage("�8�m----------------------------------------");
		}
		return false;
	}

}
