package net.souppvp.units;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import de.dytanic.cloudnet.bukkitproxy.api.CloudNetAPI;
import de.dytanic.cloudnet.servergroup.ServerState;

public class gameselector {

	public static CloudNetAPI cloudapi = CloudNetAPI.getInstance();
	
	public static void opengames() {
		
	}
	
	public static void updateinvs() {
		for (Player all : Bukkit.getOnlinePlayers()) {
			if(all.getOpenInventory().getTitle().equalsIgnoreCase("�aTeam-QSG")) {
				opengameservers(all, "Team-QSG");
			} else if(all.getOpenInventory().getTitle().equalsIgnoreCase("�cINGAME �7| �aTeam-QSG")) {
				ingameopengameservers(all, "Team-QSG");
			}
		}
	}
	
	public static void ingameopengameservers(Player p, String gametype) {
		Inventory inv = Bukkit.createInventory(null, 9*6, "�cINGAME �7| �a"+gametype);
		List<String> servers = cloudapi.getServers(gametype);
		List<String> ingame = new ArrayList<>();
		int players = 0;
		for (int i = 0; i < servers.size(); i++) {
			if(cloudapi.getServerInfo(servers.get(i)).getServerState().equals(ServerState.INGAME)){
				ingame.add(servers.get(i));
			}
			players = players + cloudapi.getServerInfo(servers.get(i)).getOnlineCount();
		}
		ItemStack i = new ItemStack(Material.NETHER_STAR);
		ItemMeta im = i.getItemMeta();
		im.setDisplayName("�c"+gametype);
		ArrayList<String> lore = new ArrayList<>();
		lore.add("�aSpieler�8: �e"+players);
		lore.add("�aServer�8: �e"+servers.size());
		im.setLore(lore);
		i.setItemMeta(im);
		inv.setItem(4, i);
		for (int j = 0; j < ingame.size(); j++) {
			inv.setItem(9+j, net.souppvp.units.inv.createItemStack("�4"+ingame.get(j), cloudapi.getServerInfo(ingame.get(j)).getOnlineCount(), (short)14, Material.STAINED_CLAY));
		}
		inv.setItem(45, net.souppvp.units.inv.createItemStack("�cZur�ck", 1, (short)0, Material.BARRIER));
		
		p.openInventory(inv);
	}
	
	public static void opengameservers(Player p, String gametype) {
		Inventory inv = Bukkit.createInventory(null, 9*6, "�a"+gametype);
		List<String> servers = cloudapi.getServers(gametype);
		List<String> free = new ArrayList<>();
		int players = 0;
		for (int i = 0; i < servers.size(); i++) {
			if(cloudapi.getServerInfo(servers.get(i)).getServerState().equals(ServerState.LOBBY)){
				free.add(servers.get(i));
			}
			players = players + cloudapi.getServerInfo(servers.get(i)).getOnlineCount();
		}
		ItemStack i = new ItemStack(Material.NETHER_STAR);
		ItemMeta im = i.getItemMeta();
		im.setDisplayName("�c"+gametype);
		ArrayList<String> lore = new ArrayList<>();
		lore.add("�aSpieler�8: �e"+players);
		lore.add("�aServer�8: �e"+servers.size());
		im.setLore(lore);
		i.setItemMeta(im);
		inv.setItem(4, i);
		for (int j = 0; j < free.size(); j++) {
			if(cloudapi.getServerInfo(free.get(j)).getOnlineCount() >= cloudapi.getServerInfo(free.get(j)).getMaxPlayers()) {
				inv.setItem(9+j, net.souppvp.units.inv.createItemStack("�6"+free.get(j), cloudapi.getServerInfo(free.get(j)).getOnlineCount(), (short)4, Material.STAINED_CLAY));
			} else {
				inv.setItem(9+j, net.souppvp.units.inv.createItemStack("�a"+free.get(j), cloudapi.getServerInfo(free.get(j)).getOnlineCount(), (short)5, Material.STAINED_CLAY));
			}
		}
		inv.setItem(45, net.souppvp.units.inv.createItemStack("�cZur�ck", 1, (short)0, Material.BARRIER));
		inv.setItem(53, net.souppvp.units.inv.createItemStack("�cIngame Server", 1, (short)0, Material.SULPHUR));
		p.openInventory(inv);
	}
	
}
