package net.souppvp.units;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;

public class hider {
	
	public static HashMap<Player, Integer> mode = new HashMap<>();
	
	public static void updatehider(Player p){
		if(lobbymanager.gethider(p.getUniqueId().toString()) == 0){
			mode.replace(p, 1);
			
			hide(p);
		} else if(lobbymanager.gethider(p.getUniqueId().toString()) == 1){
			mode.replace(p, 2);
			hide(p);
		} else if(lobbymanager.gethider(p.getUniqueId().toString()) == 2){
			mode.replace(p, 0);
			hide(p);
		}
		try {
			PreparedStatement ps = MySQL.getConnection().prepareStatement("UPDATE settings SET hider="+mode.get(p)+" WHERE UUID=?");
			ps.setString(1, p.getUniqueId().toString());
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		if(hider.mode.get(p) == 0){
			p.getInventory().setItem(1, inv.createItemStack("�aAlle Spieler Anzeigen", 1, (short)10, Material.INK_SACK));
		} else if(hider.mode.get(p) == 1){
			p.getInventory().setItem(1, inv.createItemStack("�5VIP und �cTeam-Mitglieder", 1, (short)5, Material.INK_SACK));
		} else if(hider.mode.get(p) == 2){
			p.getInventory().setItem(1, inv.createItemStack("�cKeine Spieler Anzeigen", 1, (short)1, Material.INK_SACK));
		}
	}
	
	public static void hide(Player p){
		if(mode.get(p) == 0){
			for(Player all : Bukkit.getOnlinePlayers()){
				p.showPlayer(all);
			}
		} else if(mode.get(p) == 1){
			for(Player all : Bukkit.getOnlinePlayers()){
				if(all.hasPermission("server.vip")){
					p.showPlayer(all);
				} else {
					p.hidePlayer(all);
				}
			}
		} else if(mode.get(p) == 2){
			for(Player all : Bukkit.getOnlinePlayers()){
				p.hidePlayer(all);
			}
		}
	}
	
}
