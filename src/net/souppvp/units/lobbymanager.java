package net.souppvp.units;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class lobbymanager {
	
	public static Integer getnick(String UUID){
		try {
			PreparedStatement ps = MySQL.getConnection().prepareStatement("SELECT nick FROM settings WHERE UUID = ?");
			ps.setString(1, UUID);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				return rs.getInt("nick");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static Integer gethider(String UUID){
		try {
			PreparedStatement ps = MySQL.getConnection().prepareStatement("SELECT hider FROM settings WHERE UUID = ?");
			ps.setString(1, UUID);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				return rs.getInt("hider");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static boolean UserExists(String UUID){
		try {
			PreparedStatement ps = MySQL.getConnection().prepareStatement("SELECT UUID FROM settings WHERE UUID = ?");
			ps.setString(1, UUID);
			ResultSet rs = ps.executeQuery();
			return rs.next();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

}
