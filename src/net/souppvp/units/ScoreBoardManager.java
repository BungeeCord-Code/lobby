package net.souppvp.units;

import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;import de.dytanic.cloudnet.bukkitproxy.api.CloudNetAPI;

public class ScoreBoardManager {

	private static HashMap<Scoreboard, Player> boards = new HashMap<>();
	private static 	CloudNetAPI cloudapi = CloudNetAPI.getInstance();
	
	@SuppressWarnings("deprecation")
	public static void setsb(Player p){
		Scoreboard sb = Bukkit.getScoreboardManager().getNewScoreboard();
		Team admin = sb.registerNewTeam("a");
		Team manager = sb.registerNewTeam("b");
		Team headdev = sb.registerNewTeam("c");
		Team headmod = sb.registerNewTeam("d");
		Team headbuilder = sb.registerNewTeam("e");
		Team dev = sb.registerNewTeam("f");
		Team mod = sb.registerNewTeam("g");
		Team sup = sb.registerNewTeam("h");
		Team builder = sb.registerNewTeam("i");
		Team designer = sb.registerNewTeam("j");
		Team vip = sb.registerNewTeam("k");
		Team emerlad = sb.registerNewTeam("l");
		Team diamond = sb.registerNewTeam("m");
		Team gold = sb.registerNewTeam("n");
		Team spieler = sb.registerNewTeam("o");
		Team players = sb.registerNewTeam("player");
		admin.setPrefix("§4Admin §8● §4");
		manager.setPrefix("§cManager §8● §c");
		headdev.setPrefix("§cH-Dev §8● §c");
		headmod.setPrefix("§cH-Mod §8● §c");
		headbuilder.setPrefix("§cH-Build §8● §c");
		dev.setPrefix("§cDev §8● §c");
		mod.setPrefix("§cMod §8● §c");
		sup.setPrefix("§cSup §8● §c");
		builder.setPrefix("§cBuild §8● §c");
		designer.setPrefix("§cDesigne §8● §c");
		vip.setPrefix("§5VIP §8● §5");
		emerlad.setPrefix("§aEmerald §8● §a");
		diamond.setPrefix("§bDiamond §8 §b");
		gold.setPrefix("§6Gold §8● §6");
		spieler.setPrefix("§9Spieler §8● §9");
		players.addEntry(ChatColor.AQUA.toString());
		players.setPrefix(" §e"+cloudapi.getCloudNetwork().getOnlineCount());
		String team = "§cFehler";
		if(p.hasPermission("player.admin")){
			admin.addEntry(p.getName());
			team = "§4Admin";
		} else if(p.hasPermission("player.manager")){
			manager.addEntry(p.getName());
			team = "§cManager";
		} else if(p.hasPermission("player.headdev")){
			headdev.addEntry(p.getName());
			team = "§cH-Dev";
		} else if(p.hasPermission("player.headmod")){
			headmod.addEntry(p.getName());
			team = "§cH-Mod";
		} else if(p.hasPermission("player.headbuilder")){
			headbuilder.addEntry(p.getName());
			team = "§cH-Build";
		} else if(p.hasPermission("player.dev")){
			dev.addEntry(p.getName());
			team = "§cDev";
		} else if(p.hasPermission("player.mod")){
			mod.addEntry(p.getName());
			team = "§cMod";
		} else if(p.hasPermission("player.sup")){
			sup.addEntry(p.getName());
			team = "§cSup";
		} else if(p.hasPermission("player.builder")){
			builder.addEntry(p.getName());
			team = "§cBuild";
		} else if(p.hasPermission("player.designer")){
			designer.addEntry(p.getName());
			team = "§cDesigner";
		} else if(p.hasPermission("player.vip")){
			vip.addEntry(p.getName());
			team = "§5VIP";
		} else if(p.hasPermission("player.emerald")){
			emerlad.addEntry(p.getName());
			team = "§aEmerald";
		} else if(p.hasPermission("player.diamond")){
			diamond.addEntry(p.getName());
			team = "§bDiamond";
		} else if(p.hasPermission("player.gold")){
			gold.addEntry(p.getName());
			team = "§6Gold";
		} else{
			spieler.addEntry(p.getName());
			team = "§9Spieler";
		}
		p.setDisplayName(sb.getPlayerTeam(p).getPrefix()+p.getName());
		Objective ob = sb.registerNewObjective("123", "123");
		ob.setDisplaySlot(DisplaySlot.SIDEBAR);
		ob.setDisplayName(p.getDisplayName());
		ob.getScore("§1").setScore(11);
		ob.getScore("§fOnline§8:").setScore(10);
		ob.getScore(ChatColor.AQUA.toString()).setScore(9);
		ob.getScore("§2").setScore(8);
		ob.getScore("§fRang§8:").setScore(7);
		ob.getScore(" "+team).setScore(6);
		ob.getScore("§3").setScore(5);
		ob.getScore("§fCoins§8:").setScore(4);
		ob.getScore(" §eCoins").setScore(3);
		
		p.setScoreboard(sb);
		if(boards.containsValue(p)){
			boards.replace(sb, p);
		}
		boards.put(sb, p);
		for(Player all : Bukkit.getOnlinePlayers()){
			for(Scoreboard sb1 : boards.keySet()){
				Team admin1 = sb1.getTeam("a");
				Team manager1 = sb1.getTeam("b");
				Team headdev1 = sb1.getTeam("c");
				Team headmod1 = sb1.getTeam("d");
				Team headbuilder1 = sb1.getTeam("e");
				Team dev1 = sb1.getTeam("f");
				Team mod1 = sb1.getTeam("g");
				Team sup1 = sb1.getTeam("h");
				Team builder1 = sb1.getTeam("i");
				Team designer1 = sb1.getTeam("j");
				Team vip1 = sb1.getTeam("k");
				Team emerlad1 = sb1.getTeam("l");
				Team diamond1 = sb1.getTeam("m");
				Team gold1 = sb1.getTeam("n");
				Team spieler1 = sb1.getTeam("o");
				if(all.hasPermission("player.admin")){
					admin1.addEntry(all.getName());
				} else if(all.hasPermission("player.manager")){
					manager1.addEntry(all.getName());
				} else if(all.hasPermission("player.headdev")){
					headdev1.addEntry(all.getName());
				} else if(all.hasPermission("player.headmod")){
					headmod1.addEntry(all.getName());
				} else if(all.hasPermission("player.headbuilder")){
					headbuilder1.addEntry(all.getName());
				} else if(all.hasPermission("player.dev")){
					dev1.addEntry(all.getName());
				} else if(all.hasPermission("player.mod")){
					mod1.addEntry(all.getName());
				} else if(all.hasPermission("player.sup")){
					sup1.addEntry(all.getName());
				} else if(all.hasPermission("player.builder")){
					builder1.addEntry(all.getName());
				} else if(all.hasPermission("player.designer")){
					designer1.addEntry(all.getName());
				} else if(all.hasPermission("player.vip")){
					vip1.addEntry(all.getName());
				} else if(all.hasPermission("player.emerald")){
					emerlad1.addEntry(all.getName());
				} else if(all.hasPermission("player.diamond")){
					diamond1.addEntry(all.getName());
				} else if(all.hasPermission("player.gold")){
					gold1.addEntry(all.getName());
				} else{
					spieler1.addEntry(all.getName());
				}
			}
		}
	}
	
	public static void updatesb(){
		for(Scoreboard sb : boards.keySet()){
			Team players = sb.getTeam("player");
			players.setPrefix(" §e"+cloudapi.getCloudNetwork().getOnlineCount());
		}
	}
	
}
