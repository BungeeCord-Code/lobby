package net.souppvp.units;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MySQL {

	public static String user;
	public static String pw;
	public static String port;
	public static String host;
	public static String db;
	private static Connection con;
	
	public static void Connect(){
		if (!isConnected()) {	
			try {
				con = DriverManager.getConnection("jdbc:mysql://"+host+":"+port+"/"+db+"?autoReconnect=true", user, pw);
			} catch (SQLException e) {
			}
		}
	}
	
	public static void Disconnect(){
		if (isConnected()) {	
			try {
				con.close();
			} catch (SQLException e) {
			}
		}
	}
	
	public static boolean isConnected(){
		return (con == null ? false : true);
	}
	
	public static Connection getConnection(){
		return con;
		
		
	}
}
