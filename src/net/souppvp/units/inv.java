package net.souppvp.units;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import net.souppvp.Lobby;

public class inv {
	
	public static ItemStack createItemStack(String displayname, int count, short damage, Material mat){
		
		ItemStack i = new ItemStack(mat, count, damage);
		ItemMeta im = i.getItemMeta();
		im.setDisplayName(displayname);
		i.setItemMeta(im);
		return i;
	}
	
	public static void JoinItems(Player p){
		Inventory inv = p.getInventory();
		ItemStack i = new ItemStack(Material.SKULL_ITEM, 1, (short)3);
		SkullMeta im = (SkullMeta) i.getItemMeta();
		im.setOwner(p.getName());
		im.setDisplayName("�aFreunde");
		i.setItemMeta(im);
		inv.clear();
		inv.setItem(0, createItemStack("�cSpielmodie", 1, (short)0, Material.CHEST));
		inv.setItem(5, createItemStack("�aLobby-Switcher", 1, (short)0, Material.EMERALD));
		inv.setItem(8, i);
		if(p.hasPermission("nick.nick")){
			inv.setItem(3, createItemStack("�5AutoNick �8� "+Lobby.nick.get(p), 1, (short)0, Material.NAME_TAG));
		}
		if(hider.mode.get(p) == 0){
			inv.setItem(1, createItemStack("�aAlle Spieler Anzeigen", 1, (short)10, Material.INK_SACK));
		} else if(hider.mode.get(p) == 1){
			inv.setItem(1, createItemStack("�5VIP und �cTeam-Mitglieder", 1, (short)5, Material.INK_SACK));
		} else if(hider.mode.get(p) == 2){
			inv.setItem(1, createItemStack("�cKeine Spieler Anzeigen", 1, (short)1, Material.INK_SACK));
		}
		inv.setItem(7, createItemStack("�6Gadgets", 1, (short)0, Material.JUKEBOX));
	}

}
