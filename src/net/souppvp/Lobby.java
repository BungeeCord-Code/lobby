package net.souppvp;

import java.io.File;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import net.souppvp.cmd.chatclear;
import net.souppvp.cmd.lobby;
import net.souppvp.events.Damage;
import net.souppvp.events.Hunger;
import net.souppvp.events.Interact;
import net.souppvp.events.InventoryClick;
import net.souppvp.events.Join;
import net.souppvp.events.Quit;
import net.souppvp.events.chat;
import net.souppvp.units.MySQL;
import net.souppvp.units.ScoreBoardManager;
import net.souppvp.units.gameselector;

public class Lobby extends JavaPlugin {
	
	public static HashMap<Player, String> nick = new HashMap<>();
	
	public void config(){
		try {
			if(!getDataFolder().exists()){
				getDataFolder().mkdir();
			}
			File file = new File(getDataFolder().getPath(), "config.yml");
			if(!file.exists()){
				file.createNewFile();
				FileConfiguration config = YamlConfiguration.loadConfiguration(file);
				config.set("mysql.username", "username");
				config.set("mysql.password", "password");
				config.set("mysql.port", "3306");
				config.set("mysql.host", "127.0.0.1");
				config.set("mysql.database", "db");
				config.save(file);
			}
			File file2 = new File(getDataFolder().getPath(), "locations.yml");
			if(!file2.exists()){
				file2.createNewFile();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void onEnable(){
		Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {
			
			@Override
			public void run() {
				gameselector.updateinvs();
			}
		}, 60, 60);
		Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {
			
			@Override
			public void run() {
				ScoreBoardManager.updatesb();
			}
		}, 100, 100);
		config();
		this.getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
		File file = new File(getDataFolder().getPath(), "config.yml");
		FileConfiguration config = YamlConfiguration.loadConfiguration(file);
		MySQL.user = config.getString("mysql.username");
		MySQL.pw = config.getString("mysql.password");
		MySQL.host = config.getString("mysql.host");
		MySQL.port = config.getString("mysql.port");
		MySQL.db = config.getString("mysql.database");
		MySQL.Connect();
		this.getCommand("lobbysystem").setExecutor(new lobby(this));
		this.getCommand("clearchat").setExecutor(new chatclear());
		PluginManager manager = Bukkit.getPluginManager();
		manager.registerEvents(new Join(), this);
		manager.registerEvents(new Quit(), this);
		manager.registerEvents(new Interact(), this);
		manager.registerEvents(new chat(), this);
		manager.registerEvents(new InventoryClick(this), this);
		manager.registerEvents(new Hunger(), this);
		manager.registerEvents(new Damage(), this);
		try {
			PreparedStatement ps = MySQL.getConnection().prepareStatement("CREATE TABLE IF NOT EXISTS settings(UUID VARCHAR(100), hider int(1), nick int (1))");
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
}
